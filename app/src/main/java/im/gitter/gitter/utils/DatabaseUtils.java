package im.gitter.gitter.utils;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DatabaseUtils {

    private static final String ID_COLUMN = "_id";

    public static void upsert(@NonNull SQLiteDatabase database, @NonNull String table, @NonNull Map<String, ContentValues> contentValuesById) {
        database.beginTransaction();
        try {
            Set<String> updateableIds = findUpdateableIds(database, table, contentValuesById.keySet());
            // clone the set so that we can modify contents without affecting the map
            Set<String> insertableIds = new HashSet<>(contentValuesById.keySet());
            insertableIds.removeAll(updateableIds);

            for (String id : insertableIds) {
                ContentValues values = contentValuesById.get(id);
                database.insert(table, null, values);
            }

            for (String id : updateableIds) {
                ContentValues values = contentValuesById.get(id);
                database.update(table, values, ID_COLUMN + " = ?", new String[]{id});
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    public static void replaceLookups(@NonNull SQLiteDatabase database, @NonNull String table, @NonNull String foreignKeyColumn, Set<String> foreignKeys) {
        database.beginTransaction();
        try {
            database.delete(table, null, null);
            for (String foreignKey : foreignKeys) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(foreignKeyColumn, foreignKey);
                database.insert(table, null, contentValues);
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
        }
    }

    private static Set<String> findUpdateableIds(SQLiteDatabase database, String table, Set<String> ids) {
        Set<String> updateableIds = new HashSet<>();
        String[] idArray = ids.toArray(new String[ids.size()]);

        String[] columns = new String[]{ ID_COLUMN };
        StringBuilder builder = new StringBuilder(idArray.length * 2);

        for (int i = 0; i < idArray.length; i++) {
            if (i > 0) {
                builder.append(",");
            }
            builder.append("?");
        }
        String selection = ID_COLUMN + " IN (" + builder.toString() + ")";

        Cursor cursor = database.query(table, columns, selection, idArray, null, null, null);
        int idColumnIndex = cursor.getColumnIndex(ID_COLUMN);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            String id = cursor.getString(idColumnIndex);
            updateableIds.add(id);
        }
        cursor.close();

        return updateableIds;
    }
}
