package im.gitter.gitter.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import im.gitter.gitter.R;
import im.gitter.gitter.models.CreateRoomModel;
import im.gitter.gitter.models.Group;

public class PickCommunityDialog extends DialogFragment {

    private int selected = -1;
    private ArrayAdapter<Group> communityAdapter;
    private Delegate delegate;
    private CreateRoomModel model;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        delegate = (Delegate) activity;
        model = delegate.getModel(this);
        communityAdapter = delegate.getAdapter(this);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        selected = communityAdapter.getPosition(model.getCommunity());

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.create_room_community)
                .setSingleChoiceItems(communityAdapter, selected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selected = which;
                        ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                    }
                })
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (selected >= 0) {
                            model.setCommunity(communityAdapter.getItem(selected));
                        }
                    }
                })
                .setNeutralButton(R.string.create_room_new_community, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delegate.onCreateCommunitySelected(PickCommunityDialog.this);
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .create();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AlertDialog) getDialog()).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(selected > -1);
    }

    public interface Delegate {
        CreateRoomModel getModel(PickCommunityDialog dialog);
        ArrayAdapter<Group> getAdapter(PickCommunityDialog dialog);
        void onCreateCommunitySelected(PickCommunityDialog fragment);
    }
}
